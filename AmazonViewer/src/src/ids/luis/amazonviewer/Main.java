package src.ids.luis.amazonviewer;

import java.util.Scanner;

import ids.luis.amazonviewer.model.Movie;

public class Main {
	static Scanner entrada = new Scanner(System.in);
	static int exit = 0;

	public static void main(String[] args) {
		showMenu();
	}

	public static void showMenu() {
		Movie movie = new Movie();
		movie.setTitle("Coco");
		movie.setDuration(120);

		do {
			System.out.println("!--- BIENVENIDOS A AMAZON VIEWER ---!");
			System.out.println("");
			System.out.println("Selecciona el n�mero de la opci�n deseada:");
			System.out.println("1. Movies");
			System.out.println("2. Series");
			System.out.println("3. Books");
			System.out.println("4. Magazines");
			System.out.println("0. Exit");

			System.out.print("Ingresa el n�mero de la opci�n a elegir: ");
			exit = entrada.nextInt();

			switch (exit) {
			case 0:
				System.out.println("Has salido");
				break;
			case 1:
				showMovies();
				break;
			case 2:
				System.out.println("Series");
				break;
			case 3:
				System.out.println("Books");
				break;
			case 4:
				System.out.println("Magazines");
				break;

			default:
				System.out.print("Ingresa el n�mero de la opci�n a elegir: ");
				break;
			}

		} while (exit != 0);
	}

	public static void showMovies() {
		int exit = 1;

		do {
			System.out.println("");
			System.out.println(":: MOVIES ::");
		} while (exit != 0);
	}

	public static void showSeries() {
	}

	public static void showChapters() {
	}

	public static void showBooks() {
	}

	public static void showMagazines() {
	}
}
