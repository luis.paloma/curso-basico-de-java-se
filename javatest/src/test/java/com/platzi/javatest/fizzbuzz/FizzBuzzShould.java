package com.platzi.javatest.fizzbuzz;

import org.junit.Test;

import static org.junit.Assert.*;

public class FizzBuzzShould {
//
//    fizzBuzz(3) ⇒ "Fizz"
//    fizzBuzz(6) ⇒ "Fizz"
//    fizzBuzz(5) ⇒ "Buzz"
//    fizzBuzz(10) ⇒ "Buzz"
//    fizzBuzz(15) ⇒ "FizzBuzz"
//    fizzBuzz(30) ⇒ "FizzBuzz"
//    fizzBuzz(2) ⇒ "2"
//    fizzBuzz(16) ⇒ "16"


    @Test
    public void testnumberIsdivisibleBy3() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Fizz", fizzBuzz.fizzBuz(3));
        assertEquals("Fizz", fizzBuzz.fizzBuz(6));
    }

    @Test
    public void testnumberIsdivisibleBy5() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Buzz", fizzBuzz.fizzBuz(5));
        assertEquals("Buzz", fizzBuzz.fizzBuz(10));
    }

    @Test
    public void testnumberIsdivisibleBy3And5() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("FizzBuzz", fizzBuzz.fizzBuz(15));
        assertEquals("FizzBuzz", fizzBuzz.fizzBuz(30));
    }

    @Test
    public void testnumberIsNotdivisibleByNumbers() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("FizzBuzz", fizzBuzz.fizzBuz(15));
        assertEquals("FizzBuzz", fizzBuzz.fizzBuz(30));
    }
}