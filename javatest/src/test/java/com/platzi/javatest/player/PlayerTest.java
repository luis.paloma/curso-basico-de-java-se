package com.platzi.javatest.player;

import org.junit.Test;
import org.mockito.Mockito;


import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void looses_when_dices_number_is_too_low() {
        Dice dice = new Mockito().mock(Dice.class);
        Mockito.when(dice.rol()).thenReturn(2);

        Player player = new Player(dice, 5);
        assertEquals(false, player.play());
    }
}