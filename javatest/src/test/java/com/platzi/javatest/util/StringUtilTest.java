package com.platzi.javatest.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilTest {

    @Test
    public void testRepeat() {
        assertEquals(StringUtil.repeat("hola", 3), "holaholahola");
        assertEquals(StringUtil.repeat("hola", 1), "hola");
    }

    @Test
    public void testIsEmptyAnyString() {
        assertFalse(StringUtil.isEmpty("No es vacio"));
    }

    @Test
    public void testStringIsEmpty() {
        assertTrue(StringUtil.isEmpty(""));
    }

    @Test
    public void testStringNullIsEmpty() {
        assertTrue(StringUtil.isEmpty(null));
    }

    @Test
    public void testStringWithSpacesIsEmpty() {
        assertTrue(StringUtil.isEmpty("      "));
    }




}