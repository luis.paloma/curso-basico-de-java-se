package com.platzi.javatest.movies.data;

import com.platzi.javatest.movies.model.Genre;
import com.platzi.javatest.movies.model.Movie;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class MovieRepositoryIntegrationTest {

    private MovieRepositoryJDBC movieRepositoryJDBC;
    private DriverManagerDataSource dataSource;

    @Before
    public void setUp() throws Exception {
        dataSource = new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");

        ScriptUtils.executeSqlScript(dataSource.getConnection(), new ClassPathResource("sql-scripts/test-data.sql"));

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        movieRepositoryJDBC = new MovieRepositoryJDBC(jdbcTemplate);
    }

    @Test
    public void load_all_movies() throws SQLException {
        Collection<Movie> movies = movieRepositoryJDBC.findAll();

        assertThat(movies, is(Arrays.asList(
                new Movie(1, "Dark Knight", 152, Genre.ACTION, "Christopher Nolan"),
                new Movie(2, "Memento", 113, Genre.THRILLER, "Christopher Nolan"),
                new Movie(3, "Matrix", 136, Genre.ACTION, "Sam Raimi")
        )));
    }

    @Test
    public void load_movie_by_id() {
        Movie movie = movieRepositoryJDBC.findById(2);

        assertThat(movie, is(
                new Movie(2, "Memento", 113, Genre.THRILLER, "Christopher Nolan")
        ));
    }

    @Test
    public void insert_a_movie() {
        Movie movie = new Movie( "Super 8", 112, Genre.THRILLER, "Guillermo del toro");
        movieRepositoryJDBC.saveOrUpdate(movie);
        Movie movieFromDB = movieRepositoryJDBC.findById(4);
        assertThat(movieFromDB, is(new Movie(4, "Super 8", 112, Genre.THRILLER, "Guillermo del toro")));
    }

    @Test
    public void find_movies_by_name() {
        Collection<Movie> moviesAdded = Arrays.asList(
                new Movie(1, "Dark Knight", 152, Genre.ACTION, "Christopher Nolan")
        );
        String name = "ark";
        Collection<Movie> movies = movieRepositoryJDBC.findByName(name);
        assertThat(movies, is(moviesAdded));
    }

    @Test
    public void find_movies_by_director() {
        Collection<Movie> moviesAdded = Arrays.asList(
                new Movie(1, "Dark Knight", 152, Genre.ACTION, "Christopher Nolan"),
                new Movie(2,"Memento", 113, Genre.THRILLER, "Christopher Nolan")
                );
        String name = "chris";
        Collection<Movie> movies = movieRepositoryJDBC.findByDirector(name);
        assertThat(movies, is(moviesAdded));
    }

    @Test
    public void find_movie_by_template() {
        Collection<Movie> moviesAdded = Arrays.asList(
                new Movie(1, "Dark Knight", 152, Genre.ACTION, "Christopher Nolan"),
                new Movie(2,"Matrix", 136, Genre.ACTION, "Sam Raimi")
        );
        Movie template = new Movie("",1,Genre.ACTION, "");
        Collection<Movie> movies = movieRepositoryJDBC.findMoviesByTemplate(template);
        assertThat(movies, is(moviesAdded));
    }

    @After
    public void tearDown() throws Exception {
        //Remove h2 files -- hhtps://stackoverflow.com/a/51809831/1121497
        final Statement s = dataSource.getConnection().createStatement();
        s.execute("drop all objects delete files"); //"shutdown" is also enough for mem db
    }
}