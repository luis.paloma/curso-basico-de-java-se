package com.platzi.javatest.movies.data;

import com.platzi.javatest.movies.model.Genre;
import com.platzi.javatest.movies.model.Movie;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class MovieRepositoryJDBC implements MovieRepository {

    private JdbcTemplate jdbcTemplate;

    public MovieRepositoryJDBC(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Movie findById(long id) {
        Object[] args = {id};
        return jdbcTemplate.queryForObject("select * from movies where id = ?", args, movieMapper);
    }

    @Override
    public Collection<Movie> findAll() {
        return jdbcTemplate.query("select * from movies", movieMapper);
    }

    private static RowMapper<Movie> movieMapper = (resultSet, i) ->
            new Movie(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getInt("minutes"),
                    Genre.valueOf(resultSet.getString("genre")),
                    resultSet.getString("director"));


    @Override
    public void saveOrUpdate(Movie movie) {
        jdbcTemplate.update("insert into movies (name, minutes, genre, director) values (?, ?, ?, ?)", movie.getName(), movie.getMinutes(), movie.getGenre().toString(), movie.getDirector());
    }

    public Collection<Movie> findByName(String name) {
        Object[] args = {"%" + name.toLowerCase() + "%"};
        return jdbcTemplate.query("select * from movies where LOWER(name) like ?", args, movieMapper);
    }

    public Collection<Movie> findByDirector(String director) {
        Object[] args = {"%" + director.toLowerCase() + "%"};
        return jdbcTemplate.query("select * from movies where LOWER(director) like ?", args, movieMapper);
    }

    public Collection<Movie> findMoviesByTemplate(Movie template) {
        String var = "%";
        Object[] args = {var + template.getName() + var, var + template.getMinutes() + var, template.getGenre() + var, var + template.getDirector() + var};
        return jdbcTemplate.query("select * from movies where (Lower(name) like ?) and (minutes like ?) and (genre like ?) and (director like ?)", args, movieMapper);
    }
}
