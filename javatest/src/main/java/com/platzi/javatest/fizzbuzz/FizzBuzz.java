package com.platzi.javatest.fizzbuzz;

public class FizzBuzz {
    public String fizzBuz(int i) {
        if (i % 5 == 0 && i % 3 == 0) {
            return "FizzBuzz";
        } else if (i % 5 == 0) {
            return "Buzz";
        } else if (i % 3 == 0) {
            return "Fizz";
        } else {
            return String.valueOf(i);
        }
    }
}
