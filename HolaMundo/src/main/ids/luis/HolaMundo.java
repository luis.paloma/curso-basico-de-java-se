package main.ids.luis;

//Upper Camel Case
public class HolaMundo {

	public static void main(String[] args) {
//		System.out.println("Hola Mundo");

		// Enteros
		byte edad = 127;
		short year = -32768;
		int id_User = 1001;
		long id_Twitter = 1329127368172638712L;

		// Tipos de datos con punto decimal
		float diametro = 34.25F;
		double precio = 123456.213131231231242324234234234;

		// Texto
		char genero = 'F';

		// Lógicos
		boolean isVisible = true;
		boolean funciona = false;

		int variable = 1;
		int Variable = 2;
		int _variable = 3;
		int $variable = 4;

		// Constantes
		int VALOR = 0;
		int VALOR_MAXIMO = 1;

		// Lower Camel Case
		int minValor = 4;

		// CAST
//		byte b = 6;
//		short s = b;
//
//		b = (byte) s;
//
//		int i = 1;
//		double d = 2.5;
//
//		i = (int) d;

//		int codigo = 100;
//		char codigoASCII = (char) codigo;
//
//		short numero = 300;
//		byte numeroByte = (byte) numero;

		// Arrays
		// Declaración de Arrays

//		int[] arregloInt = new int[3];
//		double[] arregloDouble;
//
//		int[][] arreglo2D = new int[2][3]; // Caben 6 cupcackes
//		int[][][] array3D = new int[3][3][2]; // Caben 18 Cookies
//		int[][][][] array4D = new int[1][2][3][4];
//
//		char[][] days = { { 'M', 'T', 'W' }, { 'M', 'T', 'W' } };
//
//		char[] names = new char[4];
//		names[0] = 'h';
//		names[1] = 'o';
//		names[2] = 'l';
//		names[3] = 'a';
//
//		char[][][][] monkey = new char[2][3][2][2];
//		monkey[1][0][0][1] = 'M';

		// Operadores Aritméticos
//		int a = 1;
//		int aa = a + a;

//		System.out.println("El valor de aa es: " + aa);

		double x = 2.56;
		int y = 9;
		float w = (float) x + y;

//		System.out.println(w*2);

//		double k = 4 / 0.00002;
//		System.out.println(k);

//		System.out.println(7 % 2);

		// Operadores de asignación
		double f = 2;
		double g = 3;
		// f=f+g;
		f -= g;
		f += g;
		f *= g;
		f /= g;
//		System.out.println(f + "\n \n \n");

		int l = 3;

		// 1._ Incrementar el valor l+1
//		System.out.println(l++);
//		System.out.println(l);

		// 2._ Asignar el valor a l
//		System.out.println(++l);

		// System.out.println(variable + "\n" + Variable + "\n" + _variable + "\n" +
		// $variable);

		int q = 5;
		int p = 4;
//		System.out.println(q != p);

//		byte c = 1;
//		byte j = 1;
//		byte x = c+j;

//		System.out.println();

		int i = 7;
		char c = 'w'; 
		System.out.println((i >= 6) && (c == 'w'));
	} 

}
